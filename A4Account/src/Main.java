import javax.swing.JOptionPane;


public class Main {
	public static void main(String[] args) {
		Account sepher = new Account();
		Account taban = new Account("Taban", 8888888888L, 1000.00);
	
		System.out.println(sepher.getBalance());
		if(sepher.withdraw(100.00)) {
			JOptionPane.showMessageDialog(null, sepher);
		} else {
			JOptionPane.showMessageDialog(null, "Insufficeint funds!\n" + sepher);
		}
		
		System.out.println("Taban initial balance is " +taban.getBalance());
		//deposit 100.00
		taban.setBalance(100.00);
		System.out.println("Taban current balance is " +taban.getBalance());
		System.out.println(taban);
	}

}
