/**
 * Account class is to use for creating new bank account object.
 * 
 * Example:
 * Account anAccount = new Acount("David", 8888888888L, "100.00");
 * anAccount.getBalance();
 *  
 * @author tkanchanawanchai6403
 *
 */
public class Account {
	private String name;
	private Long acNumber;
	private double balance;
	private final double FEE=15.00;
	
	/**
	 * Initialize the account values to default.
	 */
	public Account() {
		name="";
		acNumber= 0000000000L;
		balance =0.0;
	}
	
	/**
	 * Initialize the account values.
	 * @param name
	 * @param acNumber
	 * @param balance
	 */
	public Account(String name, Long acNumber, double balance) {
		this.name = name;
		this.acNumber = acNumber;
		this.balance = balance;
	}
	
	/**
	 * Deposit balance.
	 * @param balance the amount of money to deposit in double type 
	 */
	public void setBalance(double balance) {
		this.balance += balance;
	}
	
	/**
	 * Inquiry for the current balance.
	 * @return the current balance
	 */
	
	public double getBalance() {
		return this.balance;
	}
	
	/**
	 *
	 * Withdraw the specified amount from the current balance. 
	 * @param amount the amount to withdraw in double type
	 * @return true if the withdraw is complete otherwise false if the witdraw is fail
	 */
	public boolean withdraw (double amount) {
		if(this.balance>=amount) {
			this.balance -= amount;
			return true;
		} else { 
			return false;
		}
	}
	
	/**
	 * Returns the account information.
	 */
	public String toString() {
		return "Account name: " + this.name + "\n" + 
				"Account number: " + this.acNumber + "\n" +
				"Balance: " + this.balance + "\n";
	}
}
